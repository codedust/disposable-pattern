﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using DisposablePattern.RealExamples.OnlyUnmanaged;

namespace DisposablePattern.Cli
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                const string filename = @"c:\temp\test.txt";
                Console.WriteLine("The file: {0}", filename);
                Console.WriteLine();

                SealedClassUnmanaged sealedClassUnmanaged = new SealedClassUnmanaged(filename);

                ulong fileSize = sealedClassUnmanaged.Size;
                Console.WriteLine("The size: {0}", fileSize);
                Console.WriteLine();

                sealedClassUnmanaged.Dispose();

                string fileContent = File.ReadAllText(filename);
                Console.WriteLine("The content:");
                Console.WriteLine(fileContent);
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Pause();
            }
        }

        private static void Pause()
        {
            Console.WriteLine();
            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
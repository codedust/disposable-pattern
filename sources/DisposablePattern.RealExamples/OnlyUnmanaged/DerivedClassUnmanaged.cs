﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using DisposablePattern.Kernel32;
using DisposablePattern.Utils;
using FileAccess = DisposablePattern.Kernel32.FileAccess;
using FileAttributes = DisposablePattern.Kernel32.FileAttributes;
using FileShare = DisposablePattern.Kernel32.FileShare;

namespace DisposablePattern.RealExamples.OnlyUnmanaged
{
    public class DerivedClassUnmanaged : BaseClassUnmanaged
    {
        private bool disposed;
        private readonly IntPtr handle;

        public ulong Size2
        {
            get
            {
                uint lowerWord = Kernel32Library.GetFileSize(handle, out uint upperWord);
                return upperWord.UniteWith(lowerWord);
            }
        }

        public DerivedClassUnmanaged(string filename1, string filename2)
            : base(filename1)
        {
            if (filename2 == null) throw new ArgumentNullException(nameof(filename2));
            if (filename2 == string.Empty) throw new ArgumentException("The filename cannot be an empty string.", nameof(filename2));

            handle = Kernel32Library.CreateFile(
                filename2,
                FileAccess.GenericRead,
                FileShare.None,
                IntPtr.Zero,
                CreationDisposition.OpenExisting,
                FileAttributes.Normal,
                IntPtr.Zero);

            if (handle == new IntPtr(-1))
                throw new FileNotFoundException($"Cannot open '{filename2}'.");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            Kernel32Library.CloseHandle(handle);

            disposed = true;

            base.Dispose(disposing);
        }
    }
}
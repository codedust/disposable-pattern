﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace DisposablePattern.RealExamples.OnlyManaged
{
    public class DerivedClassManaged : BaseClassManaged
    {
        private bool disposed;
        private readonly StreamReader streamReader;

        public ulong Size => (ulong)streamReader.BaseStream.Length;

        public DerivedClassManaged(string filename1, string filename2)
            : base(filename1)
        {
            if (filename2 == null) throw new ArgumentNullException(nameof(filename2));
            if (filename2 == string.Empty) throw new ArgumentException("The filename cannot be an empty string.", nameof(filename2));

            streamReader = new StreamReader(filename2);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
                streamReader.Dispose();

            disposed = true;

            base.Dispose(disposing);
        }

        // A finalizer may be introduced here if we have unmanaged code.
    }
}
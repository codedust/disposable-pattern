﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace DisposablePattern.RealExamples.OnlyManaged
{
    public sealed class SealedClassManaged : IDisposable
    {
        private bool isDisposed;
        private readonly StreamReader streamReader;

        public ulong Size => (ulong)streamReader.BaseStream.Length;

        public SealedClassManaged(string filename)
        {
            if (filename == null) throw new ArgumentNullException(nameof(filename));
            if (filename == string.Empty) throw new ArgumentException("The filename cannot be an empty string.", nameof(filename));

            streamReader = new StreamReader(filename);
        }

        public void Dispose()
        {
            if (isDisposed)
                return;

            streamReader.Dispose();

            isDisposed = true;

            // No finalization suppressed. There will never be a finalizer.
        }

        // No protected virtual method because there are no inheritors possible.

        // No finalizer method.
    }
}
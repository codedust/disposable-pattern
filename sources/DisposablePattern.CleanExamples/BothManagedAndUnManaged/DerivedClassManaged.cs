﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.IO;

namespace DisposablePattern.CleanExamples.BothManagedAndUnmanaged
{
    public class DerivedClassManaged : BaseClass
    {
        private bool isDisposed;

        private readonly MemoryStream memoryStream = new MemoryStream();

        protected override void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
            {
                // Free any managed resources here.
                memoryStream.Dispose();
            }

            isDisposed = true;

            // Call the base class implementation.
            base.Dispose(disposing);
        }

        // No finalizer needed.
    }
}
﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Runtime.InteropServices;

namespace DisposablePattern.CleanExamples.BothManagedAndUnmanaged
{
    /// <summary>
    /// This is a base class (a class that can be inherited) that contains both
    /// managed and unmanaged resources.
    /// It must provide a mechanism to release both types of resources both manually and automated.
    /// </summary>
    public class BaseClass : IDisposable
    {
        private bool isDisposed;

        private readonly IntPtr intPtr = Marshal.AllocHGlobal(1024);
        private readonly MemoryStream memoryStream = new MemoryStream();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
            {
                // Free any managed resources here.
                memoryStream.Dispose();
            }

            // Free any unmanaged resources here.
            Marshal.FreeHGlobal(intPtr);

            isDisposed = true;
        }

        ~BaseClass()
        {
            Dispose(false);
        }
    }
}
﻿// DisposablePattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.InteropServices;

namespace DisposablePattern.CleanExamples.OnlyUnmanaged
{
    public class DerivedClassUnmanaged : BaseClassUnmanaged
    {
        private bool isDisposed;

        private readonly IntPtr intPtr = Marshal.AllocHGlobal(1024);

        protected override void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            // Free any unmanaged resources here.
            Marshal.FreeHGlobal(intPtr);

            isDisposed = true;

            // Call the base class implementation.
            base.Dispose(disposing);
        }

        // No finalizer needed. The base class provides one.
    }
}